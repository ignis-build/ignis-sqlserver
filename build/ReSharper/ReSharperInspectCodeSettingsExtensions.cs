﻿using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;

namespace ReSharper;

static class ReSharperInspectCodeSettingsExtensions
{
    public static T SetConfiguration<T>(this T settings, string value)
        where T : ReSharperInspectCodeSettings =>
        settings.SetProperty("Configuration", value);

    public static T EnableNoBuild<T>(this T settings)
        where T : ReSharperInspectCodeSettings =>
        settings.SetProcessArgumentConfigurator(args => args.Add("--no-build"));
}