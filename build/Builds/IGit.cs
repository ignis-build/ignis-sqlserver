﻿using Nuke.Common;
using Nuke.Common.Tools.GitVersion;

namespace Builds;

interface IGit : INukeBuild
{
    [GitVersion(Framework = "net6.0")] GitVersion GitVersion => TryGetValue(() => GitVersion)!;
}