﻿using System.Collections.Immutable;
using System.Linq;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

interface IPackage : ICompile
{
    AbsolutePath NuPkgDirectory => OutputDirectory / "nupkg";

    // ReSharper disable once UnusedMember.Global
    Target Pack => _ => _
        .DependsOn(Compile)
        .Executes(() =>
        {
            var projects = new[]
                {
                    "Ignis.SqlServer",
                    "Ignis.SqlServer.Tool",
                    "Ignis.Nuke.SqlServer"
                }
                .Select(project => Solution.GetProject(project))
                .ToImmutableArray();
            
            DotNetPack(s => s
                .SetConfiguration(Configuration)
                .SetOutputDirectory(NuPkgDirectory)
                .SetVersion(GitVersion.NuGetVersionV2)
                .SetProperty("EmbedUntrackedSources", true)
                .SetProperty("IncludeSymbols", true)
                .SetProperty("SymbolPackageFormat", "snupkg")
                .SetProperty("PackageLicenseExpression", "MIT")
                .SetAuthors("Tomo Masakura")
                .SetPackageProjectUrl("https://gitlab.com/ignis-build/ignis-sqlserver")
                .SetRepositoryUrl("https://gitlab.com/ignis-build/ignis-sqlserver.git")
                .SetRepositoryType("git")
                .EnableNoRestore()
                .EnableNoBuild()
                .CombineWith(projects, (s, project) => s.SetProject(project)));
        });
}