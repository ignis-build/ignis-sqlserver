﻿using System.CommandLine;
using System.Threading.Tasks;
using Ignis.SqlServer.Data;
using Ignis.SqlServer.Tool.Commands;

namespace Ignis.SqlServer.Tool
{
    internal sealed class ToolRunner
    {
        private readonly Command _command;

        public ToolRunner(IConnectionFactory connectionFactory)
        {
            _command = SqlServerCommand.Create(connectionFactory);
        }

        public int Invoke(string[] args)
        {
            return _command.Invoke(args);
        }

        // ReSharper disable once UnusedMember.Global
        public Task<int> InvokeAsync(string[] args)
        {
            return _command.InvokeAsync(args);
        }
    }
}