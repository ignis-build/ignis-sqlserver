﻿using System.CommandLine;

namespace Ignis.SqlServer.Tool.Commands.Database
{
    // ReSharper disable once ClassNeverInstantiated.Global
    // ReSharper disable UnusedAutoPropertyAccessor.Global
    internal sealed class DatabaseParameter
    {
        public static void AddTo(Command command)
        {
            command.Add(new Option<string>("--connection") {IsRequired = true});
            command.Add(new Argument("database"));
        }
#pragma warning disable 8618
        public string Connection { get; set; }
        public string Database { get; set; }
#pragma warning restore 8618
    }
}