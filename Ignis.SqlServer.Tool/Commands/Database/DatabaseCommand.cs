﻿using System.CommandLine;
using Ignis.SqlServer.Data;

namespace Ignis.SqlServer.Tool.Commands.Database
{
    internal static class DatabaseCommand
    {
        public static Command Create(IConnectionFactory connectionFactory)
        {
            return new Command("database")
            {
                DatabaseCreateCommand.Create(connectionFactory),
                DatabaseRecreateCommand.Create(connectionFactory)
            };
        }
    }
}