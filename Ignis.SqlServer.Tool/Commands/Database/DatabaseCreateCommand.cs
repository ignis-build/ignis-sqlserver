﻿using System.CommandLine;
using System.CommandLine.Invocation;
using System.Threading.Tasks;
using Ignis.SqlServer.Data;

namespace Ignis.SqlServer.Tool.Commands.Database
{
    internal static class DatabaseCreateCommand
    {
        public static Command Create(IConnectionFactory connectionFactory)
        {
            var handler = new Handler(connectionFactory);

            var command = new Command("create");
            DatabaseParameter.AddTo(command);
            command.Handler = CommandHandler.Create((DatabaseParameter parameter) => handler.InvokeAsync(parameter));
            return command;
        }

        private sealed class Handler
        {
            private readonly IConnectionFactory _connectionFactory;

            public Handler(IConnectionFactory connectionFactory)
            {
                _connectionFactory = connectionFactory;
            }

            public Task<int> InvokeAsync(DatabaseParameter parameter)
            {
                var instance = new SqlServerInstance(parameter.Connection, _connectionFactory);
                instance.Databases.Create(parameter.Database);

                return Task.FromResult(0);
            }
        }
    }
}