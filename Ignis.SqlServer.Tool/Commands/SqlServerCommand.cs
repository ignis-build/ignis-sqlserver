﻿using System.CommandLine;
using Ignis.SqlServer.Data;
using Ignis.SqlServer.Tool.Commands.Database;

namespace Ignis.SqlServer.Tool.Commands
{
    internal static class SqlServerCommand
    {
        public static Command Create(IConnectionFactory connectionFactory)
        {
            return new RootCommand
            {
                DatabaseCommand.Create(connectionFactory)
            };
        }
    }
}