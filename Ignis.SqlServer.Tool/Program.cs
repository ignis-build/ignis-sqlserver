﻿using Ignis.SqlServer.Data;

namespace Ignis.SqlServer.Tool
{
    internal static class Program
    {
        private static int Main(string[] args)
        {
            return new ToolRunner(new ConnectionFactory()).Invoke(args);
        }
    }
}