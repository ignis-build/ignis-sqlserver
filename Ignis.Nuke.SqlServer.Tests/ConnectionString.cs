﻿using System;
using Microsoft.Data.SqlClient;

namespace Ignis.Nuke.SqlServer;

internal sealed class ConnectionString
{
    private readonly string _value;

    private ConnectionString(string value)
    {
        _value = value;
    }

    private static ConnectionString? FromEnvironment()
    {
        var connectionString = Environment.GetEnvironmentVariable("SQLCONNSTR_Default");
        if (string.IsNullOrEmpty(connectionString)) return null;

        return new ConnectionString(connectionString);
    }

    private static ConnectionString Default()
    {
        var value = new SqlConnectionStringBuilder
            {
                DataSource = @".\SQLEXPRESS",
                IntegratedSecurity = true
            }
            .ToString();
        return new ConnectionString(value);
    }

    public static ConnectionString Get()
    {
        return FromEnvironment() ?? Default();
    }

    public override string ToString()
    {
        return _value;
    }

    public static implicit operator string(ConnectionString value)
    {
        return value._value;
    }
}