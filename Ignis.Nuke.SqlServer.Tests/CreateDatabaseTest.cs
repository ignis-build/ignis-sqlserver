﻿using System;
using PowerAssert;
using Xunit;
using static Ignis.Nuke.SqlServer.SqlServerTasks;

namespace Ignis.Nuke.SqlServer;

public sealed class CreateDatabaseTest : IDisposable
{
    private readonly ConnectionString _connectionString;
    private readonly Database _database;

    public CreateDatabaseTest()
    {
        _connectionString = ConnectionString.Get();

        _database = Database
            .OpenRandomName(_connectionString)
            .ForceDrop();
    }

    [Fact]
    public void TestCreateDatabase()
    {
        SqlServerCreateDatabase(s => s
            .SetConnection(_connectionString)
            .SetDatabase(_database.Name));

        PAssert.IsTrue(() => _database.Exists());
    }

    public void Dispose()
    {
        _database.Dispose();
    }
}