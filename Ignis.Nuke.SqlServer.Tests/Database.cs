﻿using System;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;

namespace Ignis.Nuke.SqlServer;

internal sealed class Database : IDisposable
{
    public string Name { get; }
    private readonly SqlConnection _connection;

    private Database(string connectionString, string name)
    {
        Name = name;
        _connection = new SqlConnection(connectionString);
    }

    public Database ForceDrop()
    {
        _connection.Execute($"drop database if exists {Name}");
        return this;
    }

    public bool Exists()
    {
        const string sql = "select name from sys.databases where name = @Name";
        return _connection.Query<string>(sql, new {Name}).Any();
    }

    public static Database OpenRandomName(string connectionString)
    {
        var name = "d" + Guid.NewGuid().ToString().Replace("-", "");
        return new Database(connectionString, name);
    }

    public void Dispose()
    {
        ForceDrop();
        _connection.Dispose();
    }
}