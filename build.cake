#addin Cake.FileHelpers

var target = Argument("target", "Pack");
var configuration = Argument("configuration", (string) null);
var packageVersion = Argument("package-version", (string) null);
var build = Argument("build", (string) null);
var output = (DirectoryPath) Argument("output", "dist");
var gitlabDeployToken = Argument("gitlab-deploy-token", (string) null);

class ArgumentsBuilder
{
    private readonly IList<Func<ProcessArgumentBuilder, ProcessArgumentBuilder>> _arguments =
        new List<Func<ProcessArgumentBuilder, ProcessArgumentBuilder>>();
    
    public ArgumentsBuilder Append(string key, string value)
    {
        if (!string.IsNullOrWhiteSpace(value))
            _arguments.Add(arguments => arguments.Append($"{key}={value}"));
    
        return this;
    }
    
    public ProcessArgumentBuilder Build(ProcessArgumentBuilder arguments)
    {
        var current = arguments;
        
        foreach (var argument in _arguments)
        {
            current = argument(current);
        }
        
        return current;
    }
}

class Version
{
    private readonly string _version;
    private readonly string _build;

    public Version(string version, string build)
    {
        _version = version;
        _build = build;
    }
    
    public string Prefix()
    {
        if (string.IsNullOrWhiteSpace(_version)) return null;

        return _version.Split('-', 2)[0];
    }
    
    public string Suffix()
    {
        if (string.IsNullOrWhiteSpace(_version)) return null;

        var parts = _version.Split('-', 2);
        return parts.Length >= 2 ? parts[1] : null;
    }
    
    public string File()
    {
        if (string.IsNullOrWhiteSpace(_build)) return Prefix();
        
        return $"{Prefix()}.{_build}";
    }

    public ProcessArgumentBuilder AppendTo(ProcessArgumentBuilder arguments)
    {
        return new ArgumentsBuilder()
            .Append("/p:VersionPrefix", Prefix())
            .Append("--version-suffix", Suffix())
            .Append("/p:FileVersion", File())
            .Build(arguments);
    }
}

var version = new Version(packageVersion, build);

Task("Test")
    .Does(() => DotNetCoreTest("./Ignis.SqlServer.Tool.Tests",new DotNetCoreTestSettings
    {
        Configuration = configuration
    }));

Task("Pack")
    .Does(() => DotNetCorePack("./Ignis.SqlServer.Tool", new DotNetCorePackSettings
    {
        ArgumentCustomization = arguments => version.AppendTo(arguments),
        Configuration = configuration,
        OutputDirectory = output
    }));

Task("Push:GitLab")
    .IsDependentOn("Pack")
    .Does(() => {
        var nugetConfig = FileReadText("nuget.config.template")
            .Replace("GITLAB_DEPLOY_TOKEN", gitlabDeployToken);
        FileWriteText(output.CombineWithFilePath("nuget.config"), nugetConfig);
            
        DotNetCoreNuGetPush("*", new DotNetCoreNuGetPushSettings
        {
            Source = "gitlab",
            WorkingDirectory = output
        });
    });

RunTarget(target);