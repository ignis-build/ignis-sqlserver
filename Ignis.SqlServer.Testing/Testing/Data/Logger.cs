﻿using System.Text;

namespace Ignis.SqlServer.Testing.Data;

public sealed class Logger
{
    private readonly StringBuilder _text = new();

    public void WriteLine(string text)
    {
        _text.AppendLine(text);
    }

    public override string ToString()
    {
        return _text.ToString();
    }
}