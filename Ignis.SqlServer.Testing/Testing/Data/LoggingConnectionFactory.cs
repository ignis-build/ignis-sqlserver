﻿using System.Data;
using Ignis.SqlServer.Data;

namespace Ignis.SqlServer.Testing.Data;

public sealed class LoggingConnectionFactory : IConnectionFactory
{
    private readonly Logger _logger;

    public LoggingConnectionFactory(Logger logger)
    {
        _logger = logger;
    }

    public IDbConnection Create()
    {
        return new LoggingDbConnection(_logger);
    }
}