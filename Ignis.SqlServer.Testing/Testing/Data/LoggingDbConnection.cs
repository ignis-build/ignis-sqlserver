﻿using System.Data;

namespace Ignis.SqlServer.Testing.Data;

internal sealed class LoggingDbConnection : IDbConnection
{
    private readonly Logger _logger;
    private string? _connectionString;

    public LoggingDbConnection(Logger logger)
    {
        _logger = logger;
    }

    public void Dispose()
    {
        Close();
    }

    public IDbTransaction BeginTransaction()
    {
        throw new NotSupportedException();
    }

    public IDbTransaction BeginTransaction(IsolationLevel il)
    {
        throw new NotSupportedException();
    }

    public void ChangeDatabase(string databaseName)
    {
        throw new NotSupportedException();
    }

    public void Close()
    {
        _logger.WriteLine("Close()");
    }

    public IDbCommand CreateCommand()
    {
        return new LoggingDbCommand(_logger);
    }

    public void Open()
    {
        _logger.WriteLine("Open()");
    }

    public string? ConnectionString
    {
#pragma warning disable CS8766
        get => _connectionString;
#pragma warning restore CS8766
        set
        {
            _connectionString = value;
            _logger.WriteLine($@"SetConnectionString(""{value}"")");
        }
    }

    public int ConnectionTimeout => throw new NotSupportedException();
    public string Database => throw new NotSupportedException();
    public ConnectionState State => throw new NotSupportedException();
}