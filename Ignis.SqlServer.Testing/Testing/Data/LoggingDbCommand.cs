﻿using System.Data;

namespace Ignis.SqlServer.Testing.Data;

internal sealed class LoggingDbCommand : IDbCommand
{
    private readonly Logger _logger;

    public LoggingDbCommand(Logger logger)
    {
        _logger = logger;
    }

    public void Dispose()
    {
    }

    public void Cancel()
    {
    }

    public IDbDataParameter CreateParameter()
    {
        throw new NotSupportedException();
    }

    public int ExecuteNonQuery()
    {
        if (CommandText == "create database exist;") throw new InvalidOperationException("データベース exist はすでに存在します。");

        _logger.WriteLine(@$"Execute(""{CommandText}"")");
        return 0;
    }

    public IDataReader ExecuteReader()
    {
        throw new NotSupportedException();
    }

    public IDataReader ExecuteReader(CommandBehavior behavior)
    {
        throw new NotSupportedException();
    }

    public object ExecuteScalar()
    {
        throw new NotSupportedException();
    }

    public void Prepare()
    {
        throw new NotSupportedException();
    }

#pragma warning disable CS8766
    public string? CommandText { get; set; }
#pragma warning restore CS8766
    public int CommandTimeout { get; set; }
    public CommandType CommandType { get; set; }
    public IDbConnection? Connection { get; set; }
    public IDataParameterCollection Parameters => throw new NotSupportedException();
    public IDbTransaction? Transaction { get; set; }
    public UpdateRowSource UpdatedRowSource { get; set; }
}