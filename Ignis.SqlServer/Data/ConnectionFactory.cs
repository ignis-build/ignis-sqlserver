﻿using System.Data;
using Microsoft.Data.SqlClient;

namespace Ignis.SqlServer.Data;

public sealed class ConnectionFactory : IConnectionFactory
{
    public IDbConnection Create()
    {
        return new SqlConnection();
    }
}