﻿using System.Data;

namespace Ignis.SqlServer.Data;

internal static class DbConnectionExtensions
{

    // ReSharper disable once UnusedMethodReturnValue.Global
    public static int ExecuteNonQuery(this IDbConnection connection, string sql)
    {
        using var command = connection.CreateCommand();
        command.CommandText = sql;
        return command.ExecuteNonQuery();
    }
}