﻿using System.Data;

namespace Ignis.SqlServer.Data;

public interface IConnectionFactory
{
    IDbConnection Create();
}