﻿using System.Data;
using Ignis.SqlServer.Data;
using Ignis.SqlServer.Database;

namespace Ignis.SqlServer;

public sealed class SqlServerInstance
{
    public SqlServerInstance(string connectionString) : this(connectionString, new ConnectionFactory())
    {
    }
 
    public SqlServerInstance(string connectionString, IConnectionFactory connectionFactory) :
        this(() => CreateConnection(connectionString, connectionFactory))
    {
    }

    private SqlServerInstance(Func<IDbConnection> createConnection)
    {
        Databases = new SqlServerDatabases(createConnection);
    }

    public SqlServerDatabases Databases { get; }

    private static IDbConnection CreateConnection(string connectionString, IConnectionFactory connectionFactory)
    {
        var connection = connectionFactory.Create();
        connection.ConnectionString = connectionString;
        connection.Open();
        return connection;
    }
}