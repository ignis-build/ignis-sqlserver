﻿using System.Data;

namespace Ignis.SqlServer.Database;

public sealed class SqlServerDatabases
{
    private readonly Func<IDbConnection> _createConnection;

    public SqlServerDatabases(Func<IDbConnection> createConnection)
    {
        _createConnection = createConnection;
    }

    public void Create(string databaseName)
    {
        using var context = DbContext(databaseName);
        context.Create();
    }

    public void Recreate(string databaseName)
    {
        using var context = DbContext(databaseName);
        context.DropIfExists();
        context.Create();
    }

    private DatabaseContext DbContext(string databaseName)
    {
        return new DatabaseContext(_createConnection(), databaseName);
    }
}