﻿using System.Data;
using Ignis.SqlServer.Data;

namespace Ignis.SqlServer.Database;

internal sealed class DatabaseContext : IDisposable
{
    private readonly IDbConnection _connection;
    private readonly string _databaseName;

    public DatabaseContext(IDbConnection connection, string databaseName)
    {
        _connection = connection;
        _databaseName = databaseName;
    }

    public void Dispose()
    {
        _connection.Dispose();
    }

    public void DropIfExists()
    {
        _connection.ExecuteNonQuery($"drop database if exists {_databaseName};");
    }

    public void Create()
    {
        _connection.ExecuteNonQuery($"create database {_databaseName};");
    }
}