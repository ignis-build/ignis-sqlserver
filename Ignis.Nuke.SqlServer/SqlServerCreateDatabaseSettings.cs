﻿namespace Ignis.Nuke.SqlServer;

public sealed class SqlServerCreateDatabaseSettings
{
    public string Connection { get; set; } = null!;
    public string Database { get; set; } = null!;
}