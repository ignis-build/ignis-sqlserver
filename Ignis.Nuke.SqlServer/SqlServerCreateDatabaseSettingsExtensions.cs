﻿namespace Ignis.Nuke.SqlServer;

public static class SqlServerCreateDatabaseSettingsExtensions
{
    public static SqlServerCreateDatabaseSettings SetConnection(this SqlServerCreateDatabaseSettings settings,
        string value)
    {
        settings.Connection = value;
        return settings;
    }

    public static SqlServerCreateDatabaseSettings SetDatabase(this SqlServerCreateDatabaseSettings settings,
        string value)
    {
        settings.Database = value;
        return settings;
    }
}