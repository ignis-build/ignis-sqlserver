﻿using Ignis.SqlServer;
using Nuke.Common.Tooling;

namespace Ignis.Nuke.SqlServer;

public static class SqlServerTasks
{
    // ReSharper disable once UnusedMethodReturnValue.Global
    public static IReadOnlyCollection<Output> SqlServerCreateDatabase(
        Configure<SqlServerCreateDatabaseSettings> configure)
    {
        return SqlServerCreateDatabase(configure(new SqlServerCreateDatabaseSettings()));
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public static IReadOnlyCollection<Output> SqlServerCreateDatabase(SqlServerCreateDatabaseSettings settings)
    {
        var instance = new SqlServerInstance(settings.Connection);
        instance.Databases.Create(settings.Database);
        
        return Array.Empty<Output>();
    }
}