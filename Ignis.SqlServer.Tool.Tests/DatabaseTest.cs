﻿using System.Threading.Tasks;
using Ignis.SqlServer.Testing.Data;
using Xunit;

namespace Ignis.SqlServer.Tool
{
    public sealed class DatabaseTest
    {
        private readonly Logger _logger;
        private readonly ToolRunner _target;

        public DatabaseTest()
        {
            _logger = new Logger();
            _target = new ToolRunner(new LoggingConnectionFactory(_logger));
        }

        [Fact]
        public async Task CreateDatabase()
        {
            await _target.InvokeAsync(new[]
            {
                "database",
                "create",
                @"--connection=""Data Source=.\SQLEXPRESS;""",
                "database1"
            });

            Assert.Equal(@"
SetConnectionString(""Data Source=.\SQLEXPRESS;"")
Open()
Execute(""create database database1;"")
Close()
".TrimStart(), _logger.ToString());
        }

        [Fact]
        public async Task CreateDatabaseAlreadyExists()
        {
            var result = await _target.InvokeAsync(new[]
            {
                "database",
                "create",
                @"--connection=""Data Source=.\SQLEXPRESS;""",
                "exist"
            });

            Assert.Equal(1, result);
        }

        [Fact]
        public async Task RecreateDatabase()
        {
            await _target.InvokeAsync(new[]
            {
                "database",
                "recreate",
                @"--connection=""Data Source=.\SQLEXPRESS;""",
                "database1"
            });

            Assert.Equal(@"
SetConnectionString(""Data Source=.\SQLEXPRESS;"")
Open()
Execute(""drop database if exists database1;"")
Execute(""create database database1;"")
Close()
".TrimStart(), _logger.ToString());
        }
    }
}